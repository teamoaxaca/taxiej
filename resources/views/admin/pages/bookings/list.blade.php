@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Reservas online</div>

                <div class="card-body">

                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">desde</th>
                                <th scope="col">hasta</th>
                                <th scope="col">Tipo de taxi</th>
                                <th scope="col">fecha</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($list as $booking)
                            <tr>
                                <th scope="row">{{$booking->id}}</th>
                                <td>{{$booking->from_address}}</td>
                                <td>{{$booking->to_address}}</td>
                                <td>{{$booking->car_type}}</td>
                                <td>{{$booking->booking_date}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5">
                                    {{ $list->links() }}
                                </td>
                            </tr>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection