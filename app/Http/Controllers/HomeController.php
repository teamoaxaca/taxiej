<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $booking_status = $request->session()->get('booking_status', 'no');

        return view('pages.home',['booking_status'=>$booking_status]);
    }

    /**
     * Saves boooking from request data 
     */

    public function make_booking(Request $request){
       
        $booking = new Booking();

        $redirect_page = $request->input('form_page');

        $validatedData = $request->validate([
            'from_address' => 'required',
            'to_address' => 'required',
            'car_type' => 'required',
            'phone' => 'required',
        ]);

        $booking->from_address = $validatedData['from_address'];
        $booking->to_address = $validatedData['to_address'];
        $booking->car_type = $validatedData['car_type'];
        //$booking->booking_date = request('booking_date');
        $booking->booking_date = date('Y-m-d h:i:s',time());
        $booking->phone = $validatedData['phone'];
        if($booking->save()){
            $request->session()->flash('booking_status', 'Servicio reservado, pronto nos pondremos en contacto con usted.');            
        }else{
            $request->session()->flash('booking_status', 'Reserva no guardada, por favor intente nuevamente!');
        }

        return redirect('/'.$redirect_page);
    }
     
}